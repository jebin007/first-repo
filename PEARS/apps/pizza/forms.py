from django import forms
from . models import Pizza, Topping

class PizzaForm(forms.ModelForm):
    class Meta:
        model = Pizza
        fields = [
            'user',
            'organization',
            'reporting_period',
            'name',
            'toppings',
        ]

class ToppingForm(forms.ModelForm):
    class Meta:
        model = Topping
        fields = [
            'toppings_name',
        ]