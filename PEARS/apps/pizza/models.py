from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

# Create your models here.

class Pizza(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,)
    organization = models.CharField(max_length=150)
    reporting_period = models.DateField(auto_now=False, auto_now_add=False)
    name = models.CharField(max_length=100)
    toppings = models.ManyToManyField('Topping')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('pizzas:detail', kwargs={'id': self.id})

class Topping(models.Model):
    toppings_name = models.CharField(max_length=150)

    def __str__(self):
        return self.toppings_name