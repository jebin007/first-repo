from django.shortcuts import render, get_object_or_404
from . models import Pizza, Topping
from django.contrib.auth.models import User
from .forms import PizzaForm, ToppingForm
from django.http import HttpResponseRedirect

# Create your views here.

def pizza_list(request):
    user_filter = request.GET.get('u')
    rp_filter = request.GET.get('rp')
    tp_filter = request.GET.get('t')
    if user_filter:
        queryset = Pizza.objects.filter(user__username=user_filter)

    elif rp_filter:
        queryset = Pizza.objects.filter(reporting_period=rp_filter)

    elif tp_filter:
        queryset = Pizza.objects.filter(toppings__toppings_name=tp_filter)
    else:
        queryset = Pizza.objects.all()
    context = {
        'pizzas': queryset,
        'users': User.objects.all(),
        'toppings': Topping.objects.all(),
    }
    return render(request, 'index.html', context)


def pizza_detail(request, id):
    instance = get_object_or_404(Pizza, id=id)
    context = {
        'instance': instance,
    }
    return render(request, 'pizza_detail.html', context)

def pizza_create(request):
    form = PizzaForm(request.POST or None)
    if rquest.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            form.save_m2m()
            return HttpResponseRedirect('/pizzas')
    context = {
        'form': form
    }
    return render(request, 'pizza_form.html', context)

def toppings_create(request):
    form = ToppingForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid:
            instance = form.save(commit=False)
            instance.save()
            return HttpResponseRedirect('/pizzas')
    context = {
        'form': form
    }
    return render(request, 'toppings_form.html', context)

def pizza_update(request, id=None):
    instance = get_object_or_404(Pizza, id=id)
    form = PizzaForm(request.POST or None, instance=instance)
    if request.method == 'POST':
        if form.is_valid:
            instance = form.save(commit=False)
            instance.save()
            form.save_m2m()
            return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        'instance': instance,
        'form': form,
    }
    return render(request, 'pizza_form.html', context)

def pizza_delete(request, id=None):
    instance = get_object_or_404(Pizza, id=id)
    instance.delete()
    messages.success(request, "Successfully Deleted!")
    return HttpResponseRedirect('/pizzas')
