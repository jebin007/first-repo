from django.conf.urls import url

from .views import pizza_list, pizza_detail, pizza_create,\
    toppings_create, pizza_update, pizza_delete

urlpatterns = [
    url(r'^$', pizza_list, name='pizzas'),
    url(r'^(?P<id>\d+)/$', pizza_detail, name='detail'),
    url(r'^create/$', pizza_create, name='pizza_create'),
    url(r'^create_toppings/$', toppings_create, name='toppings_create'),
    url(r'^(?P<id>\d+)/edit/$', pizza_update, name='update'),
    url(r'^(?P<id>\d+)/delete/$', pizza_delete, name='delete'),
]